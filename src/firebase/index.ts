import firebase from 'firebase';
import 'firebase/auth';

export type dbType = firebase.firestore.Firestore;
export type dbColType = firebase.firestore.CollectionReference<firebase.firestore.DocumentData>;
export type dbQueryType = firebase.firestore.Query<firebase.firestore.DocumentData>;
export type dbDocType =  firebase.firestore.DocumentReference<firebase.firestore.DocumentData>;
export type dbDocDataType = firebase.firestore.DocumentData;

export type dbLevelObjectType = {
  id: number,
  value: string,
};

export type firebaseError = firebase.FirebaseError;

export type dbUserType = firebase.User;

let firebaseConfig = {
  apiKey: "AIzaSyCQ2-SmFbO2MfMvV9BXKxxkkI2-qEIOEhY",
  authDomain: "jump-in-game.firebaseapp.com",
  projectId: "jump-in-game",
  storageBucket: "jump-in-game.appspot.com",
  messagingSenderId: "593413170147",
  appId: "1:593413170147:web:b32523d4580e47de44f6d6"
};

  firebase.initializeApp(firebaseConfig)

export const db: dbType = firebase.firestore();