import { Button } from '@material-ui/core';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Cell, { PropsCellType } from '../../components/cells/Cell';
import CellContent from '../../components/cells/CellContent';
import { CellContentType, CellMatrixType, CellType } from '../../components/cells/types/cell-type';
import letConstructorAddUnitToCell from '../../store/constructor/action/let-constructor-add-unit-to-cell/let-constructor-add-unit-to-cell';
import letConstructorChangeSelectedUnit from '../../store/constructor/action/let-constructor-change-selected-unit/let-constructor-change-selected-unit';
import { appStateType } from '../../store/store';
import {comboConstructorPutUnitToCell} from './../../store/constructor/action/action-combine/comboConstructorPutUnitToCell'

function ConstructorSpecCellWrap(props: PropsCellType) {
  const [active, setActive] = React.useState<boolean>(false);
  let selectedUnit: CellContentType | null =  useSelector((state: appStateType) => state.constructorReducer.selectedUnit);
  let matrix: CellMatrixType =  useSelector((state: appStateType) => state.constructorReducer.matrix);
  const dispatch = useDispatch();

  let content = [...props.cell.content]
  let unitIndex = content.findIndex((unit) => {
    return unit.name !== 'hole'
  })

  // React.useEffect(() => {
  //   if(active && !!selectedUnit) {
  //     setActive(() => {
  //       return false
  //     }) 
  //   }
  // }, [selectedUnit])

 

  let onClick = (e: React.MouseEvent<HTMLElement>, cell: CellType) => {
    e.stopPropagation();
    e.preventDefault();

    console.log(content);

    if(!!selectedUnit) {
      comboConstructorPutUnitToCell(dispatch, matrix, cell, selectedUnit)
    } else if(!!!selectedUnit && unitIndex > -1){
      setActive((prevActive) => {
        return !prevActive
      })
    }
  }

  let clear = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();

    if(unitIndex > -1 && !!!selectedUnit) {
      let unit: CellContentType = content[unitIndex];
  
      dispatch(letConstructorChangeSelectedUnit(unit))
      dispatch(letConstructorAddUnitToCell(matrix, props.cell, unit, false));

  
      content.splice(unitIndex, 1);
      setActive(() => {
        return false
      })
    }
  }

  return (
    <>
      <Cell {...props} onClick = {onClick}  >
        <>
        <CellContent content = {props.cell.content} />
        {
          active && unitIndex > -1 && 

          <Button  
            variant="contained"
            color="secondary"
            size="small"
            onClick={clear}>
            Clear
          </Button>
        }
        </>
      </Cell>
    </>
  )
}

export default ConstructorSpecCellWrap