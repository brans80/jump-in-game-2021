
import { useDispatch, useSelector } from "react-redux"
import letConstructorChangeSelectedUnit from "../store/constructor/action/let-constructor-change-selected-unit/let-constructor-change-selected-unit";
import { appStateType } from "../store/store"

const useClearSelectedUnit = () => {
  let selectedUnit =  useSelector((state: appStateType) => state.constructorReducer.selectedUnit);
  // const dispatch = useDispatch()

  // if(selectedUnit) {
  //   dispatch(letConstructorChangeSelectedUnit(null));
  // }

  return selectedUnit
}

export default useClearSelectedUnit