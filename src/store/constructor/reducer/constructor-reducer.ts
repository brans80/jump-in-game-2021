import { CellContentType, CellMatrixType } from "../../../components/cells/types/cell-type";
import { emptyMatrix } from "../../constants/empty-matrix";
import { constructorActionTypes, LET_CONSTRUCTOR_ADD_UNIT_TO_CELL, LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT } from "../action/consts";

let getEmptyMatrix = () => {
  return [...emptyMatrix]
}

export type ConstructorRdcType = {
  matrix: CellMatrixType,
  selectedUnit: CellContentType | null,
}

const initialState: ConstructorRdcType = {
  matrix: getEmptyMatrix(),
  selectedUnit: null
}

 let constructorReducer = (state = initialState, action: constructorActionTypes) => {
  switch (action.type) {

  case LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT: {
    let selectedUnit = action.selectedUnit; 
    return { ...state, selectedUnit };
  }

  case LET_CONSTRUCTOR_ADD_UNIT_TO_CELL: {
    let matrix = action.matrix; 
    return { ...state, matrix };
  }

  default:
    return state
  }
}

export default constructorReducer