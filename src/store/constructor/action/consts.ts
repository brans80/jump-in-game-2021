import { CellContentType, CellMatrixType } from "../../../components/cells/types/cell-type";

export const LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT = "LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT";
export const LET_CONSTRUCTOR_ADD_UNIT_TO_CELL = "LET_CONSTRUCTOR_ADD_UNIT_TO_CELL";


type letConstructorChangeSelectedUnit = {
  type: typeof LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT,
  selectedUnit: CellContentType | null,
};

type letConstructorAddUnitToCell = {
  type: typeof LET_CONSTRUCTOR_ADD_UNIT_TO_CELL,
  matrix: CellMatrixType,
};


export type constructorActionTypes = letConstructorChangeSelectedUnit | letConstructorAddUnitToCell