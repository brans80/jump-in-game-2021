
import { CellContentType, CellMatrixType, CellType } from "../../../../components/cells/types/cell-type"
import { addContentToCellAndReturnMatrix } from "../action-helpers/addContentToCellAndReturnMatrix"
import { constructorActionTypes, LET_CONSTRUCTOR_ADD_UNIT_TO_CELL } from "../consts"



let letConstructorAddUnitToCell = (matrix: CellMatrixType, cell: CellType, unit: CellContentType, add: boolean = true): constructorActionTypes => {

  let action: constructorActionTypes = {
    type: LET_CONSTRUCTOR_ADD_UNIT_TO_CELL,
    matrix: addContentToCellAndReturnMatrix(matrix, cell, unit, add)
  }

  return action
}

export default letConstructorAddUnitToCell
