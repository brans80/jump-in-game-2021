import { unitNameType } from "../../../../components/cells/types/basic-unit-type"
import { CellContentType } from "../../../../components/cells/types/cell-type"

export let findUnitByNameInCellContent = (cellContent: CellContentType[], unitName: unitNameType) => {
  return  !!cellContent.find((unitInContent) => {
    return unitInContent.name === unitName
  })
}