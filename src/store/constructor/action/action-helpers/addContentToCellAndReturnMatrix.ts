import { CellContentType, CellMatrixType, CellType } from "../../../../components/cells/types/cell-type";

export let addContentToCellAndReturnMatrix = (matrix: CellMatrixType, cell: CellType, unit: CellContentType, add: boolean ): CellMatrixType => {

  let cellCoords = cell.coords;
  let newMatrix = [...matrix];
  let newMatrixCell = {...newMatrix[cellCoords.y][cellCoords.x]};

  if(add) {
    let newContent = [...newMatrixCell.content, unit];

    newMatrixCell = {...newMatrixCell, content: [...newMatrixCell.content, unit]}
    newMatrixCell.content = newContent
    newMatrix[cellCoords.y][cellCoords.x] = newMatrixCell
  } else {
    let newContent = [...newMatrixCell.content];
    let i = newContent.findIndex((u) => {
      return u.id === unit.id
    })

    newContent.splice(i, 1);
    newMatrixCell = {...newMatrixCell, content: newContent};
    newMatrix[cellCoords.y][cellCoords.x] = newMatrixCell
  }


  return newMatrix
}