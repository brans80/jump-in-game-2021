import { CellContentType } from "../../../../components/cells/types/cell-type";
import { findUnitByNameInCellContent } from "./findUnitByNameInCellContent";

export let checkCellToAllowToUnitAdd = (cellContent: CellContentType[], selectedUnit: CellContentType): boolean => {

  let selectedUnitName = selectedUnit.name;
  let isSelectedUnitRabbit = (selectedUnitName === 'rabbit');
  let isSelectedUnitFox = (selectedUnitName === 'fox');
  let isSelectedUnitMushroom = (selectedUnitName === 'mushroom');
  let isRabbitUnitExisting = false;
  let isFoxUnitExisting = false;
  let isMushroomUnitExisting = false;
  let isHoleUnitExisting = false;
  let isAnyUnitExisting = (isRabbitUnitExisting || isFoxUnitExisting || isMushroomUnitExisting || isHoleUnitExisting);

  if(cellContent.length === 0) {
    return true
  } else {
    isRabbitUnitExisting = findUnitByNameInCellContent(cellContent, 'rabbit');
    isFoxUnitExisting = findUnitByNameInCellContent(cellContent, 'fox');
    isMushroomUnitExisting = findUnitByNameInCellContent(cellContent, 'mushroom');
    isHoleUnitExisting = findUnitByNameInCellContent(cellContent, 'hole');
  }

  if( isSelectedUnitRabbit && isAnyUnitExisting ) {
    return false
  } else if (isSelectedUnitRabbit && isHoleUnitExisting && cellContent.length === 1) {
    return true
  };

  if( isSelectedUnitFox && isAnyUnitExisting ) {
    return false
  }

  if( isSelectedUnitMushroom && isAnyUnitExisting ) {
    return false
  } else if (isSelectedUnitMushroom && isHoleUnitExisting && cellContent.length === 1) {
    return true
  }

  return false
}