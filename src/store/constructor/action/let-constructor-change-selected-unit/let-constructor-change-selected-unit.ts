
import { CellContentType } from "../../../../components/cells/types/cell-type"
import { constructorActionTypes, LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT } from "../consts"

let letConstructorChangeSelectedUnit = (selectedUnit: (CellContentType | null)): constructorActionTypes => {

  let action: constructorActionTypes = {
    type: LET_CONSTRUCTOR_CHANGE_SELECTED_UNIT,
    selectedUnit
  }

  return action
}

export default letConstructorChangeSelectedUnit
