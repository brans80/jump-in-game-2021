import { CellContentType, CellMatrixType, CellType } from "../../../../components/cells/types/cell-type";
import { appDispatchType } from "../../../store";
import { checkCellToAllowToUnitAdd } from "../action-helpers/checkCellToAllowToUnitAdd";
import letConstructorAddUnitToCell from "../let-constructor-add-unit-to-cell/let-constructor-add-unit-to-cell";
import letConstructorChangeSelectedUnit from "../let-constructor-change-selected-unit/let-constructor-change-selected-unit";

export let comboConstructorPutUnitToCell = (dispatch: appDispatchType, matrix: CellMatrixType, cell: CellType, selectedUnit: CellContentType) => {

  if(checkCellToAllowToUnitAdd(cell.content, selectedUnit)) {
    dispatch(letConstructorAddUnitToCell(matrix, cell, selectedUnit));
    dispatch(letConstructorChangeSelectedUnit(null));
  }

}