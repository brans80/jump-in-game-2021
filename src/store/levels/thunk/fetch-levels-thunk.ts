
import { DbLevelType, LevelType } from "../../../components/levels/types/levelType";
import { db, firebaseError } from "../../../firebase";
import { fireConverter } from "../../../firebase/converter";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import { catchErrorHandler } from "../../user/user-thunk/user-thunk-helpers/catchErrorHandler";

export let fetchLevels = async (dispatch: appDispatchType, toLevelId: number = 10, limit: number = 10): Promise < LevelType[] | void> => {
  var USERS = db.collection("LEVELS").orderBy("id").where("id", "<=", toLevelId).limit(limit).withConverter(fireConverter < DbLevelType > ());

  // dispatch(letChangePreloadingStatus(true))
  dispatch(letChangePreloadingStatus('levels', 'FETCHING_LEVELS', true))

  return USERS.get().then((querySnapshot) => {

    let levels: LevelType[] = [];
  
    querySnapshot.forEach((item) => {

      let dbLevel = item.data();

      let level: LevelType = {...dbLevel, value: JSON.parse(dbLevel.value)};

      levels = [...levels, level]

    })

    dispatch(letChangePreloadingStatus('levels', 'FETCHING_LEVELS', false))
    return levels


    // dispatch(letChangePreloadingStatus(false))
  


  }).catch((error: firebaseError) => {
    catchErrorHandler(dispatch, error);
    dispatch(letChangePreloadingStatus('levels', 'FETCHING_LEVELS', false))
  });
}