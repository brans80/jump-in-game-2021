
import { DbLevelType, LevelType } from "../../../components/levels/types/levelType";
import { db, firebaseError } from "../../../firebase";
import { fireConverter } from "../../../firebase/converter";
import { getStartLevels, setStartLevels } from "../../constants/levels";
import { appDispatchType, appStateType } from "../../store";
import { catchErrorHandler } from "../../user/user-thunk/user-thunk-helpers/catchErrorHandler";
import letChangeLevels from "../action/let-change-levels/let-change-levels";
import { fetchLevels } from "./fetch-levels-thunk";

export let changeLevelsByFetchedLevels = async (dispatch: appDispatchType): Promise<void> => {
  if(getStartLevels().length < 1) {
    let fetchedLevels: void | LevelType[] = await fetchLevels(dispatch);

    if(!!fetchedLevels ) {
      dispatch(letChangeLevels(fetchedLevels));
      setStartLevels(fetchedLevels)
    }
  }

}