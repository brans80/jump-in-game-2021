import { isReturnStatement } from "typescript";
import { LevelType } from "../../../components/levels/types/levelType";
import { appStateType } from "../../store";

export let getLevels = (state: appStateType): LevelType[] => {
  return state.levelsReducer.levels
}