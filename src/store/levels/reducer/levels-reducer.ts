import { LevelType } from "../../../components/levels/types/levelType"
import { LET_CHANGE_CURRENT_LEVEL, LET_CHANGE_LEVELS, LevelsActionTypes } from "../action/consts"

export type LevelsRdcType = {
  levels: LevelType[],
  currentLevel: LevelType | null
}

const initialState: LevelsRdcType = {
  levels: [],
  currentLevel: null
}

 let levelsReducer = (state = initialState, action: LevelsActionTypes) => {
  switch (action.type) {

  case LET_CHANGE_LEVELS: {
    let levels = action.levels; 
    return { ...state, levels };
  }

  case LET_CHANGE_CURRENT_LEVEL: {
    let currentLevel = action.currentLevel;
    return { ...state, currentLevel }
  }

  default:
    return state
  }
}

export default levelsReducer