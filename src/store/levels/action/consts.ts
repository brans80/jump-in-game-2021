import { LevelType } from "../../../components/levels/types/levelType";

export const LET_DO_SOMETHING = "LET_DO_SOMETHING";
export const LET_CHANGE_LEVELS = "LET_CHANGE_LEVELS";
export const LET_CHANGE_CURRENT_LEVEL = "LET_CHANGE_CURRENT_LEVEL";

type letChangeLevels = {
  type: typeof LET_CHANGE_LEVELS,
  levels: LevelType[],
};

type letChangeCurrentLevel = {
  type: typeof LET_CHANGE_CURRENT_LEVEL,
  currentLevel: LevelType,
}

export type LevelsActionTypes =  letChangeLevels | letChangeCurrentLevel