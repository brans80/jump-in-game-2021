import { LevelType } from "../../../../components/levels/types/levelType"
import { LET_CHANGE_CURRENT_LEVEL, LevelsActionTypes } from "../consts"

let letChangeCurrentLevel= (currentLevel: LevelType): LevelsActionTypes => {
  let action: LevelsActionTypes = {
    type: LET_CHANGE_CURRENT_LEVEL,
    currentLevel
  }

  return action
}

export default letChangeCurrentLevel