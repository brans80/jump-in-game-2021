import { LevelType } from "../../../../components/levels/types/levelType"
import { LET_CHANGE_LEVELS, LevelsActionTypes } from "../consts"

let letChangeLevels= (levels: LevelType[]): LevelsActionTypes => {
  let action: LevelsActionTypes = {
    type: LET_CHANGE_LEVELS,
    levels: levels
  }

  return action
}

export default letChangeLevels

