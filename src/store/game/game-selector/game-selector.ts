import { createSelector } from 'reselect';
import { AlertObjectType } from '../../../components/alert/alert-type';
import { appStateType } from '../../store';

export let getPreloadingStatus = (state: appStateType): boolean => {
  return state.gameReducer.preloadingStatus
}

export let getAlert = (state: appStateType):  null | AlertObjectType => {
  return state.gameReducer.alert
}