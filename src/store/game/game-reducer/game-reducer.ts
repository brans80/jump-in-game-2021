import { AlertObjectType } from "../../../components/alert/alert-type";
import { DbLevelType } from "../../../components/levels/types/levelType";
import { PreloaderType } from "../../../components/preloader/types/preloader-type";

import { gameActionTypes, LET_CHANGE_ALERT, LET_CHANGE_PRELOADING_STATUS } from "../game-action/game-action-constants";
import { preloaderMap } from "./consts/preloader-map";



export type gameRdcType = {
  preloadingStatus: boolean,
  alert: null | AlertObjectType,
  preloaders: PreloaderType

};

const initialState: gameRdcType = {
  preloadingStatus: true,
  alert: null,
  preloaders: preloaderMap

};

let gameReducer = (state = initialState, action: gameActionTypes) => {
  switch (action.type) {

    case LET_CHANGE_PRELOADING_STATUS: {
      let preloaders = action.preloaders
      return { ...state, preloaders }
    }

    case LET_CHANGE_ALERT: {
      let alert = action.alert
      return { ...state, alert }
    }

    default:
      return state
    }
}

export default gameReducer;