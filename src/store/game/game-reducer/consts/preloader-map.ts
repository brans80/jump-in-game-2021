import { PreloaderType } from "../../../../components/preloader/types/preloader-type";

export let preloaderMap: PreloaderType = new Map([
  ["user", []],
  ["levels", []],
  ["userDetails", []]
])

// preloaderMap.set('user', []).set('levels', []).set('userDetails', []);