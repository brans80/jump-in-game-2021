import { PreloaderItemType, PreloaderType } from "../../../../components/preloader/types/preloader-type";
import store, { appStateType } from "../../../store";
import { gameActionTypes, LET_CHANGE_PRELOADING_STATUS } from "../game-action-constants";


let letChangePreloadingStatus = (preloaderItem: PreloaderItemType, operationName: string, add: boolean): gameActionTypes => {

  let preloaders: PreloaderType = new Map(store.getState().gameReducer.preloaders);

  let item = preloaders.get(preloaderItem);

  if(item) {
    if(add) {
      item = [...item, operationName]
    } else {
      item = item.filter((oper: string) => {
        return oper !== operationName
      })
    }

    preloaders.set(preloaderItem, item)
  }



  let action: gameActionTypes = {
    type: LET_CHANGE_PRELOADING_STATUS,
    preloaders
  }

  return action
}

export default letChangePreloadingStatus;