import { AlertObjectType } from "../../../components/alert/alert-type";
import { PreloaderType } from "../../../components/preloader/types/preloader-type";


export const LET_CHANGE_PRELOADING_STATUS = 'LET_CHANGE_PRELOADING_STATUS';
export const LET_CHANGE_ALERT = 'LET_CHANGE_ALERT';

type letChangePreloadingStatusActionType = {
  type: typeof LET_CHANGE_PRELOADING_STATUS,
  preloaders: PreloaderType,
};

type letChangeAlertActionType = {
  type: typeof LET_CHANGE_ALERT,
  alert: null | AlertObjectType,
};

export type gameActionTypes = letChangePreloadingStatusActionType | letChangeAlertActionType;