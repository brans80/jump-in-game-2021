
import { AlertObjectType } from "../../../../components/alert/alert-type";
import { gameActionTypes, LET_CHANGE_ALERT } from "../game-action-constants";

export default (alert: null | AlertObjectType): gameActionTypes => {
  let action: gameActionTypes = {
    type: LET_CHANGE_ALERT,
    alert
  }
  return action
}