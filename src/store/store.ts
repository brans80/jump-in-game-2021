import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import userReducer from './user/user-reducer/user-reducer';
import gameReducer from './game/game-reducer/game-reducer';
import levelsReducer from './levels/reducer/levels-reducer';
import constructorReducer from './constructor/reducer/constructor-reducer'

export let rootReducer = combineReducers({
  userReducer,
  gameReducer,
  levelsReducer,
  constructorReducer
})

type rootReducerType = typeof rootReducer;
export type appStateType = ReturnType<rootReducerType>;
export type appDispatchType = typeof store.dispatch;

let store = createStore(rootReducer, applyMiddleware(thunk));

export default store;