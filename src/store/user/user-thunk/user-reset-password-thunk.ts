import firebase from "firebase";
import { AlertObjectType } from "../../../components/alert/alert-type";
import { formInputObjectType } from "../../../components/form/formType";
import letChangeAlert from "../../game/game-action/let-change-alert/let-change-alert";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import { catchErrorHandler, forceAlertUnfilledForm } from "./user-thunk-helpers/catchErrorHandler";
import { findInputObjectByName } from "./user-thunk-helpers/findInputObjectByName";

export let userResetPassword = (dispatch: appDispatchType, inputs: formInputObjectType[]) => {

  let email = findInputObjectByName('email', inputs);
  let currentUserEmail = email?.value;

  // dispatch(letChangePreloadingStatus(true));

  if (currentUserEmail) {
    firebase.auth().sendPasswordResetEmail(currentUserEmail).then(function () {
      let alertObject: AlertObjectType = {
        title: 'Reset current password',
        text: `Email to reset current password  was sent to your mail: ${currentUserEmail}`,
        severity: 'success',
      }

      // dispatch(TwinFoxTypefalse));

      dispatch(letChangeAlert(alertObject))
    }).catch(function (error) {

      catchErrorHandler(dispatch, error)
    });
  } else {
    forceAlertUnfilledForm(dispatch);
  }
}
