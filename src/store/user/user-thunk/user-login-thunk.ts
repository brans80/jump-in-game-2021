import firebase from "firebase";
import { formInputObjectType } from "../../../components/form/formType";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";

import { fetchToChangeUserDetails } from "./fetch-to-channge-user-details-thunk";
import { catchErrorHandler, forceAlertUnfilledForm } from "./user-thunk-helpers/catchErrorHandler";
import { findInputObjectByName } from "./user-thunk-helpers/findInputObjectByName";

export let userLogin = (dispatch: appDispatchType, inputs: formInputObjectType[]) => {
  let email = findInputObjectByName('email', inputs);
  let password = findInputObjectByName('password', inputs);

  if (email && password && email.value && password.value) {
    // dispatch(letChangePreloadingStatus(true))

    firebase.auth().signInWithEmailAndPassword(email.value, password.value)
      .then((userCredential) => {
        var user = userCredential.user;
  
        // dispatch(letChangeCurrentUser(user));
        fetchToChangeUserDetails(dispatch, user )

        // dispatch(letChangePreloadingStatus(false));


      })
      .catch((error: firebase.FirebaseError) => {
        catchErrorHandler(dispatch, error)
      });
  } else {
    forceAlertUnfilledForm(dispatch)
  }

}