import { userDetailsType } from "../../../components/user/userType";
import { db, dbUserType, firebaseError } from "../../../firebase";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import letChangeUserDetails from "../user-action/let-change-user-derails/let-change-user-details";
import { catchErrorHandler, forceAlertNoSuchDoc } from "./user-thunk-helpers/catchErrorHandler";

let fetchUserDetails = async(dispatch: appDispatchType, userId: string): Promise<userDetailsType | void> => {
  var docUserRef = db.collection("USERS").doc(userId);


  return docUserRef.get().then( (doc) => {
      if (doc.exists) {
          let userDetails: userDetailsType =  doc.data() as userDetailsType;

         
          return userDetails
      } else {
          // doc.data() will be undefined in this case
          // forceAlertNoSuchDoc(dispatch)
          let error =  new Error()
          error.name = 'No such document';
          error.message = 'No such document. Perhaps this document is not exosted in data base';
          throw error;
      }
  }).catch((error: firebaseError) => {
    catchErrorHandler(dispatch, error)
  });
}

export let fetchToChangeUserDetails = async(dispatch: appDispatchType, user: dbUserType | null) => {

  if(!!user) {
    let userDetails = await fetchUserDetails(dispatch, user?.uid as string);
    dispatch(letChangeUserDetails(userDetails as userDetailsType))

  } else {
    dispatch(letChangeUserDetails(null));
  }
}
