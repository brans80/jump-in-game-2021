import { AlertObjectType } from "../../../components/alert/alert-type";
import { userDetailsType } from "../../../components/user/userType";
import { db, dbUserType, firebaseError } from "../../../firebase";
import letChangeAlert from "../../game/game-action/let-change-alert/let-change-alert";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import { catchErrorHandler } from "./user-thunk-helpers/catchErrorHandler";

export let createAccountDetails = (dispatch: appDispatchType, user: dbUserType, displayName: string) => {
  let newUser: userDetailsType = {
    email: user.email,
    displayName,
    photo: null,
  }
  // dispatch(letChangePreloadingStatus(false));
  db.collection("USERS").doc(user.uid).set(newUser)
    .then((docRef) => {
      let alertObject: AlertObjectType = {
        title: `Account (${user.uid}) was created`,
        text: `Success! You have created an account ${user?.email}`,
        severity: "success",
      }

      // dispatch(letChangePreloadingStatus(false));
      dispatch(letChangeAlert(alertObject))
    })
    .catch((error: firebaseError) => {
      catchErrorHandler(dispatch, error)
    });
}