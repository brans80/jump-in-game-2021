import firebase from "firebase";
import { AlertObjectType } from "../../../components/alert/alert-type";
import letChangeAlert from "../../game/game-action/let-change-alert/let-change-alert";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import { catchErrorHandler } from "./user-thunk-helpers/catchErrorHandler";

export let userLogoutThunk = (dispatch: appDispatchType) => {

      // dispatch(TwinFoxTypetrue));

      firebase.auth().signOut().then((res) => {


      }).catch((error: firebase.FirebaseError) => {
        catchErrorHandler(dispatch, error)
    });
  }
