import firebase from "firebase";
import { AlertObjectType } from "../../../components/alert/alert-type";
import { formInputObjectType } from "../../../components/form/formType";
import { dbUserType, firebaseError } from "../../../firebase";
import letChangeAlert from "../../game/game-action/let-change-alert/let-change-alert";
import letChangePreloadingStatus from "../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../store";
import { createAccountDetails } from "./user-create-account-details-thunk";
import { catchErrorHandler, forceAlert, forceAlertUnfilledForm } from "./user-thunk-helpers/catchErrorHandler";
import { findInputObjectByName } from "./user-thunk-helpers/findInputObjectByName";

export let createAccount = (dispatch: appDispatchType, inputs: formInputObjectType[]) => {
  let email = findInputObjectByName('email', inputs);
  let password = findInputObjectByName('password', inputs);
  let displayName = findInputObjectByName('displayName', inputs);

  if (email && password && displayName) {
    // dispatch(letChangePreloadingStatus(true))

    firebase.auth().createUserWithEmailAndPassword(email.value, password.value)
    .then((userCredential) => {
      var user = userCredential.user;

      createAccountDetails(dispatch, user as dbUserType, (displayName as formInputObjectType).value)
    })
    .catch((error: firebaseError) => {
      catchErrorHandler(dispatch, error)
    })
  } else {

    forceAlertUnfilledForm(dispatch)
  }

}