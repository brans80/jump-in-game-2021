import { AlertObjectType } from "../../../../components/alert/alert-type";
import { firebaseError } from "../../../../firebase";
import letChangeAlert from "../../../game/game-action/let-change-alert/let-change-alert";
import letChangePreloadingStatus from "../../../game/game-action/let-change-preloading-status/let-change-preloading-status";
import { appDispatchType } from "../../../store";

export let catchErrorHandler = (dispatch: appDispatchType, error: firebaseError | Error) => {
  // dispatch(letChangePreloadingStatus(false));

  let alertObject: AlertObjectType = {
    title: (error as firebaseError).code ? (error as firebaseError).code : error.name,
    text: error.message,
    severity: "warning",
  }

  dispatch(letChangeAlert(alertObject))
}

export let forceAlert = (dispatch: appDispatchType, alertObject: AlertObjectType) => {
  // dispatch(letChangePreloadingStatus(false));
  dispatch(letChangeAlert(alertObject))
}

export let forceAlertUnfilledForm = (dispatch: appDispatchType) => {
  let alertObject: AlertObjectType = {
    title: 'input field is empty',
    text: 'fill all input fields',
    severity: "warning",
  }

  forceAlert(dispatch, alertObject)
}

export let forceAlertNoSuchDoc = (dispatch: appDispatchType) => {
  let alertObject: AlertObjectType = {
    title: 'No such document',
    text: 'No such document. Perhaps this document is not exosted in data base',
    severity: "warning",
  }

  forceAlert(dispatch, alertObject)
}