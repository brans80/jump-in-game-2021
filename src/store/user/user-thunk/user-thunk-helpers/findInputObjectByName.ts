import { formInputObjectType } from "../../../../components/form/formType";

export function findInputObjectByName(name: string, array: formInputObjectType[]): formInputObjectType | undefined {
  let val = array.find((inp) => {
    return inp.name === name;
  })
  return val
}