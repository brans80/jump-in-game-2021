import { userDetailsType } from "../../../../components/user/userType";
import { LET_CHANGE_USER_DETAILS, userActionTypes } from "../user-action-constants";

export default (userDetails: userDetailsType | null): userActionTypes => {
  let action: userActionTypes = {
    type: LET_CHANGE_USER_DETAILS,
    userDetails
  }

  return action
}

