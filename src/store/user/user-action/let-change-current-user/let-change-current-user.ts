import { dbUserType } from "../../../../firebase";
import { LET_CHANGE_CURRENT_USER, userActionTypes } from "../user-action-constants";

export default (currentDbUser: dbUserType | null): userActionTypes => {
  let action: userActionTypes = {
    type: LET_CHANGE_CURRENT_USER,
    currentUser: currentDbUser
  }

  return action
}


