import { userDetailsType } from "../../../components/user/userType";
import { dbUserType } from "../../../firebase";

export const LET_CHANGE_CURRENT_USER = "LET_CHANGE_CURRENT_USER";
export const LET_CHANGE_USER_DETAILS = "LET_CHANGE_USER_DETAILS";

type letChangeCurrentUserActionType = {
  type: typeof LET_CHANGE_CURRENT_USER,
  currentUser: dbUserType | null,
};

type letChangeUserDetailsActionType = {
  type: typeof LET_CHANGE_USER_DETAILS,
  userDetails: userDetailsType | null,
}

export type userActionTypes = letChangeCurrentUserActionType | letChangeUserDetailsActionType