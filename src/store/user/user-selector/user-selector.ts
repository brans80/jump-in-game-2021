import { createSelector } from 'reselect';
import { userDetailsType } from '../../../components/user/userType';
import { dbUserType } from '../../../firebase';
import { appStateType } from '../../store';

export let getUser = (state: appStateType): dbUserType | null => {
  return state.userReducer.currentUser
}

export let getUserDetails = (state: appStateType): userDetailsType | null | undefined => {
  return state.userReducer.userDetails
}