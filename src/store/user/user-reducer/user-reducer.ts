import { userDetailsType } from "../../../components/user/userType"
import { dbUserType } from "../../../firebase"
import { LET_CHANGE_CURRENT_USER, LET_CHANGE_USER_DETAILS, userActionTypes } from "../user-action/user-action-constants"

export type userRdcType = {
  currentUser: dbUserType | null,
  userDetails: userDetailsType | null | undefined
}

const initialState: userRdcType = {
  currentUser: null,
  userDetails: undefined,
}

 let userReducer = (state = initialState, action: userActionTypes) => {
  switch (action.type) {

  case LET_CHANGE_CURRENT_USER: {
    let currentUser = action.currentUser
    return { ...state, currentUser }
  }

  case LET_CHANGE_USER_DETAILS: {
    let userDetails = action.userDetails

    console.log(userDetails)
    return { ...state, userDetails }
  }
    

  default:
    return state
  }
}

export default userReducer