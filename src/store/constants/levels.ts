import { LevelType } from "../../components/levels/types/levelType";

let startLevels: LevelType[] = [];
export let setStartLevels = (newStartLevels: LevelType[]) => {
  startLevels = newStartLevels
}

export let getStartLevels = () => {
  return startLevels;
}