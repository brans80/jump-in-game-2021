import { CellContentType, CellMatrixType, CellRowType, CellType } from "../../components/cells/types/cell-type";
import { CoordsType } from "../../components/cells/types/coords-type";
import { HoleType } from "../../components/cells/types/hole-type";

let cellsInLine = 5;

let holeCoordsArray: CoordsType[] = [
  {y: 0, x: 0},
  {y: 0, x: cellsInLine-1},
  {y: Math.floor(cellsInLine/2), x: Math.floor(cellsInLine/2)},
  {y: cellsInLine-1, x: 0},
  {y: cellsInLine-1, x: cellsInLine-1},
]
let holeId = 0;

export let emptyMatrix : CellMatrixType = [...new Array(cellsInLine)].map((row: CellRowType, yIndex) => {

  let cellsRow: CellRowType = [...new Array(cellsInLine)].map((cell: CellType, xIndex) => {
    let holeCoords = holeCoordsArray.find((coords) => {
      return (yIndex === coords.y) && (xIndex === coords.x);
    })

    console.log(holeCoords)

    let content: CellContentType[] = [];
    if (holeCoords) {
      holeId ++;

      let newHoleObj: HoleType = {
        id: 'hole_' + holeId.toString(),
        name: 'hole',
      }
      content.push(newHoleObj)
    }

    return {
      id: 'y-'+yIndex+'_x-'+xIndex,
      coords: { x: xIndex, y: yIndex},
      content,
    }
  })
  return cellsRow
})

