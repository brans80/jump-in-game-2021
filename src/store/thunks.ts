import { userLogin } from "./user/user-thunk/user-login-thunk";
import { createAccountDetails } from "./user/user-thunk/user-create-account-details-thunk";
import { createAccount} from "./user/user-thunk/user-create-account-thunk";
import { userResetPassword} from "./user/user-thunk/user-reset-password-thunk";
import { fetchLevels } from "./levels/thunk/fetch-levels-thunk";
import { changeLevelsByFetchedLevels } from "./levels/thunk/change-levels-by-fetched-levels-thunk"

export default {
  userLogin,

  userResetPassword,
  createAccountDetails,
  createAccount,

  fetchLevels,
  changeLevelsByFetchedLevels


}