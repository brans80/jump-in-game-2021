import {  makeStyles } from '@material-ui/core';
import React from 'react';
import UserAuthStateWatcher from './components/user/UserAuthStateWatcher';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { routes } from './router/routes';
import 'fontsource-roboto';
import { useSelector } from 'react-redux';
import { appStateType } from './store/store';
import Preloader from './components/preloader/Preloader';
import PreloaderWrap from './components/preloader/PreloaderWrap';
import ModalWithAlertWrap from './components/modal/ModalWithAlertWrap';
import { PreloaderType } from './components/preloader/types/preloader-type';
import { SmallPreloaderWrap } from './components/preloader/SmallPreloaderWrap';
import ThingsContent, { ThingsArray } from './components/cells/TestWrap';

const useStyles = makeStyles({
  root: {
    fontFamily: "Roboto",
    backgroundColor: '#8eba74'
  },
});

function App() {
  const classes = useStyles();
  const user = useSelector((state: appStateType) => {
    return state.userReducer.currentUser
  });

  const IsPreloading = !!useSelector((state: appStateType) => {
    return state.userReducer.userDetails === undefined;
  });

  console.log(IsPreloading);

  return (
    <div className={classes.root}>
      <ModalWithAlertWrap/>
      {/* <PreloaderWrap/> */}
      <UserAuthStateWatcher/>
      <Router>
        <Switch>
          {
            routes.map((route) => {
              return (
                IsPreloading ? <Preloader key={route.url} diameter={200}/> : <Route key={route.url} path={route.url} component={route.pageComponent} exact={route.exact} /> 
                    
              )
            })
          }
        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
