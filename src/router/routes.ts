import AccountPage from "../components/page/AccountPage";
import AdminPage from "../components/page/AdminPage";
import CompetitorResultsPage from "../components/page/CompetitorResultsPage";
import GameSettingsPage from "../components/page/GameSettingsPage";
import HomePage from "../components/page/HomePage";
import { routeType } from "./routerTypes";
import { dbUserType } from "../firebase";
import GamePage from "../components/page/GamePage";
import store from "../store/store";
import ConstructorPage from "../components/page/ConstructorPage";

let doDisabledIfUserNull = (user: dbUserType) => {
  return !user
}

export let routes: routeType[] = [
  {
    url: "/",
    linkName: "home",
    alias: "home",
    pageComponent: HomePage,
    exact: true,
  },
  {
    url: `/account/:id`,
    linkName: "account",
    alias: "account",
    pageComponent: AccountPage,
    exact: true,
    doDisabled: doDisabledIfUserNull
  },
  {
    url: "/game-settings",
    linkName: "game-settings",
    alias: "game-settings",
    pageComponent: GameSettingsPage,
    exact: true,
    doDisabled: doDisabledIfUserNull
  },
  {
    url: "/competitor-results",
    linkName: "competitor results",
    alias: "competitor-results",
    pageComponent: CompetitorResultsPage,
    exact: true,

  },
  {
    url: "/admin",
    linkName: "admin",
    alias: "admin",
    pageComponent: AdminPage,
    exact: true,
    doDisabled: doDisabledIfUserNull
  },
  {
    url: "/game",
    linkName: "game",
    alias: "game",
    pageComponent: GamePage,
    exact: true,
    doDisabled: doDisabledIfUserNull,
    offMenu: true,
  },

  {
    url: "/level-constructor",
    linkName: "level constructor",
    alias: "constructor",
    pageComponent: ConstructorPage,
    exact: true,
    doDisabled: doDisabledIfUserNull,
    offMenu: true,
  },
]