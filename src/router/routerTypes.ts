import { FC } from "react";

export type routeType = {
  url: string;
  linkName: string;
  alias: string;
  pageComponent: FC;
  exact: boolean;
  doDisabled?: (a: any) => boolean;
  offMenu?: boolean
}