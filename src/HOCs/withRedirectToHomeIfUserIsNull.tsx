import React, { FC } from 'react'
import { useSelector } from 'react-redux'
import { appStateType } from '../store/store'
import { Redirect, useHistory } from 'react-router-dom'

export default function withRedirectToHomeIfUserIsNull(WrappedComponent: FC): FC {
  const HocComponent = () => {
    const user = useSelector((state: appStateType) =>{ 
      return state.userReducer.userDetails
    })

    let history = useHistory();

    // React.useEffect(() => {
    //   if(user === null) {

    //     history.push('/');

    //   }
    // }, [user])

    return (
      !user ? <Redirect to='/'/> :<WrappedComponent />
      // <WrappedComponent />
    )
  }

  return HocComponent
}
