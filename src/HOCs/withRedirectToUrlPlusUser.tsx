import React, { FC } from 'react'
import { useSelector } from 'react-redux'
import { appStateType } from '../store/store'
import { useHistory } from 'react-router-dom'

export default function withRedirectToUrlPlusUser(WrappedComponent: FC, ): FC {
  const HocComponent = () => {

    const user = useSelector((state: appStateType) => state).userReducer.userDetails?.email
    const history = useHistory();
    
    React.useEffect(() => {
      if(user) {
        history.push(`/account/${user}`);
      }
    }, [])

    return (
      user ? <WrappedComponent/> : <></>
    )
  }

  return HocComponent
}
