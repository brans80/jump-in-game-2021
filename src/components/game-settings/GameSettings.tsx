
import React from 'react'
import { makeStyles } from "@material-ui/core";
import { connect, useDispatch, useSelector } from "react-redux";
import { Button } from "@material-ui/core";
import { appDispatchType, appStateType } from '../../store/store';
import LevelChoice from './../levels/LevelChoice';
import { changeLevelsByFetchedLevels } from '../../store/levels/thunk/change-levels-by-fetched-levels-thunk';
import Preloader from '../preloader/Preloader';
import { routeType } from '../../router/routerTypes';
import { routes } from '../../router/routes';
import { NavLink } from 'react-router-dom';
import { LevelType } from '../levels/types/levelType';
import letChangeCurrentLevel from '../../store/levels/action/let-change-current-level/let-change-current-level';

const useStyles = makeStyles((theme) => ({
  gameSettings: {
    position: 'relative',
    width: '100%',
    minHeight: '10rem',
    display: 'block',
    boxSizing: 'border-box',
    background: '#8eba74',
    color: theme.palette.common.white,
    fontSize: '1.8rem',
    flexDirection: 'column',
  },
}));

type JigGameSettingsPanelPropsType = {

}

 function GameSettings(props: JigGameSettingsPanelPropsType) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const levels = useSelector((state: appStateType) => state.levelsReducer.levels);
  const levelsPreloader = useSelector((state: appStateType) => state.gameReducer.preloaders.get('levels'));
  const currentLevel = useSelector((state: appStateType) => state.levelsReducer.currentLevel);

  const gameRoute: routeType = routes.find((item) => {
    return item.alias === "game"
  }) as routeType

  React.useEffect(() => {
    changeLevelsByFetchedLevels(dispatch)
  }, [])

  let onChooseLevel = (level: LevelType) => {
    dispatch(letChangeCurrentLevel(level))
  }

  return (
    <div className={classes.gameSettings}>
      { 
        (levelsPreloader?.length === 0) ? <LevelChoice currentLevel = {currentLevel} levels = {levels} onChoose={(level: LevelType) => onChooseLevel(level)}/> : <Preloader diameter={75} />
      
      }
      {
      (levelsPreloader?.length === 0) && 
        <div>

          {!!currentLevel &&
          <Button
              variant="contained"
              color="primary"
              size="large"
              component={ NavLink }
              to={gameRoute.url}
              key={gameRoute.url}
              exact={gameRoute.exact}
            >
              Start to game chosen level
            </Button>}
        </div>
      }
    </div>
  )
}

const mapStateToProps = (state: appStateType) => {
  return {
    // matrix: getCellsMatrix(state),
    // levels: getGameLevels(state)
  }
}

const mapDispatchToProps = (dispatch: appDispatchType) => {
    return {
      // chooseLevel: (index: number) => dispatch(letChangeCurrentGameLevelAC(index)),
      // changeGameStatus: () => dispatch(letChangeGameStatus(2))
    }
  
}

export default connect(mapStateToProps, mapDispatchToProps)(GameSettings)