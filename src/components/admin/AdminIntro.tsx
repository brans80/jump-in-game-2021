import { Button } from '@material-ui/core'
import React from 'react'
import { NavLink } from 'react-router-dom'
import { routeType } from '../../router/routerTypes'
import { routes } from '../../router/routes'


export default function AdminIntro() {
  const constructorRoute: routeType = routes.find((item) => {
    return item.alias === "constructor"
  }) as routeType
  

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        size="large"
        component={ NavLink }
        to={constructorRoute.url}
        key={constructorRoute.url}
        exact={constructorRoute.exact}
      >
        Start to game chosen level
      </Button>
    </div>
  )
}
