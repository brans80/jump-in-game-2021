import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { BarrierType } from './types/barrier-type';

const mushroomImg = require('./../../images/jig/mushroom.png').default;
const foxImg = require('./../../images/jig/fox.png').default;

const useStyles = makeStyles((theme) => ({
  jigBarrier: {
    height: '90%',
    width: '90%',
    // backgroundImage: `url(${mushroomImg})`,
    backgroundSize: '90%',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.success.light,
    borderRadius: '50%',
    border: '2px solid  #555',
    transition: 'all 0.2s',

    '&.mushroom': {
      backgroundImage: `url(${mushroomImg})`,
    },

    '&.fox': {
      backgroundColor: '#fff',
      backgroundImage: `url(${foxImg})`,
    },

    '&.is-active': {
      height: '63%',
      width: '63%',
      backgroundColor: 'red',

    }


  },
}));

export default function Barrier(props: BarrierType) {
  const classes = useStyles();

  let modClass = props.name;
  let isActive = props.active
  
  return (
    <div className={classes.jigBarrier +' '+ modClass+(isActive ? ' is-active' : '')}>
    </div>
  )
}
