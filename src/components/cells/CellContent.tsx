import React, { FC } from 'react'
import Barrier from './Barrier'
import Rabbit from './Rabbit'
import { CellContentType } from './types/cell-type'
import { HoleType } from './types/hole-type'

function Hole(props: HoleType) {
  return (
    <>

    </>
  )
}

let contentMap = {
  'hole': Hole,
  'rabbit': Rabbit,
  'fox': Barrier,
  'mushroom': Barrier,
}



export default function CellContent(props: {content: CellContentType[]}) {
  return (

    <>
      {
        props.content.map((item) => {
          let CurComp =  contentMap[item.name] as FC<CellContentType>
          return <CurComp key={item.name + item.id} {...item}/>
        })
      }
    </>
  )
}
