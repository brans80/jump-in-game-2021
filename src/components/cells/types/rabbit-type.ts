import { basicUnitType } from "./basic-unit-type";
import { CoordsType } from "./coords-type";

export const RABBIT = 'rabbit';

export type RabbitIdType = string ;

export type RabbitType = {
  id: string,
  name: typeof RABBIT,
  active: boolean,
  coords: CoordsType
}
