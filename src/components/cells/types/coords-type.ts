export type CoordsType = {
  y: number,
  x: number,
}