import { basicUnitType, unitNameType } from "./basic-unit-type";

export const HOLE = 'hole';


export type HoleType =  
 {
    id: string,
    name: 'hole',
  }