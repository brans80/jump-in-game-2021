export type unitNameType = 'hole' | 'rabbit' | 'fox' | 'mushroom'

export type basicUnitType = {
  id: string,
  name: unitNameType
}