import { BarrierType } from "./barrier-type";
import { CoordsType } from "./coords-type";
import { HoleType } from "./hole-type";
import { RabbitType } from "./rabbit-type";

export type CellContentType = BarrierType | HoleType | RabbitType;
// export type CellContentType = any

export type CellType = {
  id: string,
  coords: CoordsType,
  content: CellContentType[]
}

export type CellRowType = CellType[];

export type CellMatrixType = CellRowType[];

