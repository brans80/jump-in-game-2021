import { basicUnitType } from "./basic-unit-type";
import { CoordsType } from "./coords-type";

export const FOX = 'fox';
export const MUSHROOM = 'mushroom';


export type BarrierNameType =  typeof FOX | typeof MUSHROOM;

export type BarrierType =  {
  id: string,
  name: BarrierNameType,
  twinId?: string,
  active: boolean,
  coords: CoordsType
};

