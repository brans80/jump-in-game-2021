import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { RabbitType } from './types/rabbit-type';

const logo = require('./../../images/jig/rabbit.png').default;

const useStyles = makeStyles((theme) => ({
  rabbit: {
    height: '90%',
    width: '90%',
    backgroundImage: `url(${logo})`,
    backgroundSize: '90%',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.background.paper,
    borderRadius: '50%',
    border: '2px solid  #ccc',
    transition: 'all 0.2s',

    '&.is-active': {
      height: '95%',
      width: '95%',
      backgroundColor: 'red',

    }
  },
}));

export default function Rabbit(props: RabbitType) {
  const classes = useStyles();
  let isActive = props.active
  return (
    <div className={classes.rabbit + (isActive ? ' is-active' : '')}>
    </div>
  )
}
