import React, { FC } from 'react'

type CarType = {
  id: string,
  type: 'car',
  speed: number,
  power: number
}

type PersonType = {
  id: string,
  type: 'person',
  age: number,
  gender: 'male' | 'female'
}

type OceanType = {
  id: string,
  type: 'ocean',
  area: number,
}

type Thingstype = CarType | PersonType | OceanType

let ThingsMap = {
  'car': Car,
  'person': Person,
  'ocean': Ocean
}

export let ThingsArray: Thingstype[] = [
  {
    id: 'pacific ocean',
    type: 'ocean',
    area: 25000
  }
]

export  function Car(props: CarType) {
  return (
    <div>
      
    </div>
  )
}

export  function Person(props: PersonType) {
  return (
    <div>
      
    </div>
  )
}

export  function Ocean(props: OceanType) {
  return (
    <div>
      
    </div>
  )
}

export default function ThingsContent (content: Thingstype[] ) {
  return (
    <>
      {content.map((thing) => {
        let ThingComponent = ThingsMap[thing.type] as FC<Thingstype>

        return <ThingComponent {...thing}/>
      })}
    </>
 
  )
}
