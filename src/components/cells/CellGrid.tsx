import { makeStyles } from '@material-ui/core';
import React from 'react';
import { PropsCellType } from './Cell';
import { CellRowType, CellMatrixType, CellType } from './types/cell-type';

const useStyles = makeStyles((theme) => ({
  jigGrid: {
    background: theme.palette.common.white,
    width: '80vmin',
    height: '80vmin',
    border: '1px solid  #111',
    display: 'flex',
    flexWrap: 'wrap',
    boxSizing: 'border-box',
    padding: '3px',
    margin: '0 auto',
  },

  matrixCellItem: {
    border: '1px solid  #111',
    width: (20) + '%',
    height: (20) + '%',
    boxSizing: 'border-box',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& .id-label': {
      zIndex: '1',
      display: 'block',
      position: 'absolute',
      fontSize: '10px',
      right: '3px',
      top: '3px',
      color: theme.palette.common.black,
    },
  }
}));

type GridPropsType = {
  matrix: CellMatrixType,
  cellComponent: (props: PropsCellType) => JSX.Element
}

function CellGrid(props: GridPropsType) {
  const classes = useStyles();

  return (
    <div className={classes.jigGrid}>
      {
        props.matrix.map((cellsRow: CellRowType) => {
          return cellsRow.map((cell: CellType) => {
              return (
                <div className={classes.matrixCellItem} key={cell.id} >
                  <span className='id-label'>
                    {cell.id}
                  </span>
                  <props.cellComponent cell = {cell} />
                </div>
              )
            })
        })
      }
    </div>
  )
}

export default CellGrid;