import { makeStyles } from '@material-ui/core';
import React from 'react'
import { BarrierType } from './types/barrier-type';
import { CellContentType, CellType } from './types/cell-type';

const holeImg = require('./../../images/jig/hole.png').default;

const useStyles = makeStyles((theme) => ({
  cell: {
    position: 'relative',
    background: '#8eba74',
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '&.hole': {
      backgroundSize: '300%',
      backgroundPosition: '46% 0',
      backgroundRepeat: 'no-repeat',
      backgroundImage: `url(${holeImg})`,
    },

    '&.possibleRabbitStep': {
      backgroundColor: '#f1b1f8',
    },

    '&.possibleFoxStep': {
      backgroundColor: '#ff8133',
    }
  },
}));

export type PropsCellType = {
  cell: CellType,
  onClick?: Function,
  children?: JSX.Element,
}

export default React.memo(function Cell(props: PropsCellType) {
  const classes = useStyles();
  const hole: CellContentType | undefined = props.cell.content.find((item: CellContentType) => {
    return item?.name === 'hole';
  })

  let clickHandler = (e: React.MouseEvent<HTMLElement>) => {
    if(!!props.onClick) {
      props.onClick(e, props.cell)
    }
  }
  
  return (
    
    <div className={classes.cell + (hole ? ' hole' : '')}  onClick={clickHandler}>
      {
        props.children &&
        props.children
      }
    </div>
  )
})
