import { BarrierType } from "../types/barrier-type";

export let mushroomsCollection: BarrierType[] = [
  {
    id: 'm1',
    name: "mushroom",
    active: false,
    coords: {y: -1, x: -1},
  },
  {
    id: 'm2',
    name: "mushroom",
    active: false,
    coords: {y: -1, x: -1},
  },
  {
    id: 'm3',
    name: "mushroom",
    active: false,
    coords: {y: -1, x: -1},
  },
]

export let foxCollection: BarrierType[] = [
  {
    id: 'fox-1-1',
    twinId: 'fox-1-2',
    name: "fox",
    active: false,
    coords: {y: -1, x: -1},
  },
  {
    id: 'fox-2-1',
    twinId: 'fox-2-2',
    name: "fox",
    active: false,
    coords: {y: -1, x: -1},
  },
]