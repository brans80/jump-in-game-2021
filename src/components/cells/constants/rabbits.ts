
import { RabbitIdType, RabbitType } from "../types/rabbit-type";

export let rabbitsCollection: RabbitType[] = [
  {
    id: 'rabbit_1',
    name: "rabbit",
    active: false,
    coords: {y: -1, x: -1}
  },
  {
    id: 'rabbit_2',
    name: "rabbit",
    active: false,
    coords: {y: -1, x: -1}
  },
  {
    id: 'rabbit_3',
    name: "rabbit",
    active: false,
    coords: {y: -1, x: -1}
  },
]

export let rabbitCap = {...rabbitsCollection[0]}


export let getRabbitObject = (id: RabbitIdType) => {
  return rabbitsCollection.find((rabbit: RabbitType) => {
    return rabbit.id === id
  })
}