import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { appDispatchType, appStateType } from '../../store/store';
import { getAlert } from '../../store/game/game-selector/game-selector';
import { connect, useDispatch, useSelector } from 'react-redux';
import letChangeAlert from '../../store/game/game-action/let-change-alert/let-change-alert';
import { AlertObjectType } from '../alert/alert-type';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #999',
      // boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }),
);

export function ModalWithAlertWrap() {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const dispatch = useDispatch()

  const alert = useSelector((state: appStateType) => state.gameReducer.alert);
  const isOpen = !!alert;

  let handleClose = () => {
    dispatch(letChangeAlert(null))
  }

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">{alert?.title}</h2>
      <p id="simple-modal-description">
      {alert?.text}
      </p>
    </div>
  );

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {body}
    </Modal>

  );
}

export default ModalWithAlertWrap;
