import { CellMatrixType } from "../../cells/types/cell-type"


export type DbLevelType = {
  id: number,
  value: string,
}

export type LevelType = {
  id: number,
  value: CellMatrixType
}

export type levelsMapType = Map<number, CellMatrixType>


