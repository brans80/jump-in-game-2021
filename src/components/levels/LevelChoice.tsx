import { makeStyles } from '@material-ui/core';
import React from 'react'
import { useDispatch } from 'react-redux';
import { getStartLevels } from '../../store/constants/levels';
import thunks from '../../store/thunks';
import { LevelType } from './types/levelType';

const useStyles = makeStyles((theme) => ({

  levelChoiceBlock: {
    position: 'relative',
    display: 'flex',
    marginBottom: '2rem',
    alignItems: 'center',
    justifyContent: 'center',
    border: '1px solid #fff',
    minHeight: '5rem'
  },
  
  levelChoiceItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '4rem',
    height: '4rem',
    border: '1px solid #fff',
    fontSize: '1.5rem',
    lineHeight: '1',
    cursor: 'pointer',

    '&:hover': {
      background: '#fff',
      opacity: 0.5,
      color: '#000',
    },

    '&.isChosen': {
      background: '#000',
      color: '#fff',
      pointerEvents: 'none',
    }
  }
}));

type PropsType = {
  levels: LevelType[],
  onChoose?: Function,
  currentLevel: LevelType | null,
}

 function LevelChoice(props: PropsType) {
  const classes = useStyles();

  let clickHandler = (level: LevelType) => {
    props.onChoose && props.onChoose(level);
  }

  return (
    <>
    <h4>level choice</h4>
    <div className={classes.levelChoiceBlock}>

      {
        props.levels.map((level: LevelType, index) => {
          let isCurrenntLevel = (props.currentLevel && props.currentLevel.id === level.id);
          return (
            <div 
              key={level.id} 
              className={classes.levelChoiceItem + (isCurrenntLevel ? ' isChosen' : '')} 
              onClick={() => clickHandler(level)}

            >
              {level.id}
            </div>
          )
        })
      }
      
    </div>
    </>
  )
}

LevelChoice.defaultProps = {
  levels: [],

}

export default LevelChoice