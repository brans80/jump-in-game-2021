import { formInputObjectType } from "./formType";

export let resetFormInputs: formInputObjectType[] = [
  {
    id: "authEmail",
    name: "email",
    value: "",
    type: "email",
    label: "email"
  },
]

export let inputs: formInputObjectType[] = [
  ...resetFormInputs,
  {
    id: "authPassword",
    name: "password",
    value: "",
    type: "password",
    label: "password"
  },
]

export let createAccountInputs: formInputObjectType[] = [
  {
    id: "displayName",
    name: "displayName",
    value: "",
    type: "text",
    label: "name"
  },
  ...inputs,

]