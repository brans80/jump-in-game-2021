import React, { ReactNode } from 'react';
import { Button, makeStyles, TextField } from '@material-ui/core';
import { formInputObjectType } from './formType';

const useStyles = makeStyles((theme) => ({
  authRoot: {
    background: '#fff',
    // width: '24rem',
    minHeight: '12rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    boxSizing: 'border-box',
    padding: '1rem',
    marginBottom: '1.5rem'
  },
}));

type formPropsType = {
  submitButtonName?: ReactNode,
  submitMethod?: (inputs: formInputObjectType[]) => void,
  inputs: formInputObjectType[]
}

const Form = (props: formPropsType) => {
  type inputsArrayType = typeof props.inputs;
  const classes = useStyles();
  const [formInputs, setFormInputs] = React.useState<inputsArrayType>([])

  React.useEffect(() => {

  }, [formInputs])

  React.useEffect(() => {
    setFormInputs(props.inputs)
  }, [])

  let changeInputValue = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, name: string) => {
    let value = e.target.value;
    let fieldName = e.target.name;

    setFormInputs((prevInputs: inputsArrayType) => {
      let newInputs =  prevInputs.map((input, i) => {
        if(input.name===fieldName) {
          input = {...input, value}
        } else {
          input = {...input}
        }
        return input
      })

      return newInputs
    })
  }

  let doSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
      e.stopPropagation();
      e.preventDefault();

      if(props.submitMethod) {
        e.stopPropagation();
        e.preventDefault();

        props.submitMethod(formInputs)
      };
  }

  return (
    <form className={classes.authRoot} noValidate autoComplete="on" onSubmit={doSubmit}>
      {props.submitButtonName &&
        <h3>{props.submitButtonName}</h3>
      }
      {
        formInputs.map( (inpObj: formInputObjectType, index) => {
          return <TextField value={inpObj.value} id={inpObj.id} label={inpObj.label} type={inpObj.type} name={inpObj.name} key={inpObj.id} onChange={(e) => changeInputValue(e, inpObj.name)}/>
        })
      }

      <hr/>
      <Button variant="contained" color="primary"  type="submit">
        {props.submitButtonName &&
        props.submitButtonName}
      </Button>


    </form>    
  )
}

Form.defaultProps = {
  submitButtonName: 'submit',
}

export default Form

