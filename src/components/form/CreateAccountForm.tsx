import React from 'react'
import { useDispatch } from 'react-redux';
import thunks from '../../store/thunks';
import { createAccount } from '../../store/user/user-thunk/user-create-account-thunk';
import Form from './Form'
import { createAccountInputs } from './formConstants';
import { formInputObjectType } from './formType'

export default function CreateAccoutnForm() {
  const dispatch = useDispatch();

  let submit = (inputs: formInputObjectType[]) => {
    createAccount(dispatch, inputs)
  }

  return (
    <Form submitButtonName='Create new account' inputs={createAccountInputs} submitMethod={(inputs: formInputObjectType[]) => submit(inputs)}/>
  )
}
