export type formInputObjectType = {
  id: string,
  name: string,
  label: string,
  value: string,
  type: string
}