import React from 'react'
import { useDispatch } from 'react-redux';
import thunks from '../../store/thunks';
import Form from './Form'
import { resetFormInputs } from './formConstants';
import { formInputObjectType } from './formType'

export default function ResetPasswordForm() {

  const dispatch = useDispatch();
  let submit = (inputs: formInputObjectType[]) => {
    thunks.userResetPassword(dispatch, inputs)
  }

  return (
    <Form submitButtonName='Reset password' inputs={resetFormInputs} submitMethod={(inputs: formInputObjectType[]) => submit(inputs)}/>
  )
}
