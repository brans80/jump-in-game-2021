import React from 'react'
import { useDispatch } from 'react-redux';
import { rootReducer } from '../../store/store';
import thunks from '../../store/thunks';

import Form from './Form'
import { inputs } from './formConstants';
import { formInputObjectType } from './formType'

export default function LoginForm() {
  const dispatch = useDispatch()
  let submit = (inputs: formInputObjectType[]) => {
    thunks.userLogin(dispatch, inputs)
  }

  return (
    <Form submitButtonName='Log in' inputs={inputs} submitMethod={(inputs: formInputObjectType[]) => submit(inputs)}/>
  )
}
