import { connect } from 'react-redux'
import React from 'react'
import { getUser, getUserDetails } from '../../store/user/user-selector/user-selector'
import MainMenu from '../main-menu/MainMenu'
import { dbUserType } from '../../firebase'
import { appStateType } from '../../store/store'
import UserLogoutBlock from '../user/UserLogoutBlock'
import { userDetailsType } from '../user/userType'

type propsType = {
  userDetails?: userDetailsType | null
}


 function Header(props: propsType) {
  return (
    <header>
      {!!props.userDetails && <UserLogoutBlock/>}
      <MainMenu userDetails = {props.userDetails}/>
    </header>
  )
}

const mapStateToProps = (state: appStateType) => ({
  userDetails: getUserDetails(state)
})


export default connect(mapStateToProps, null)(Header)

