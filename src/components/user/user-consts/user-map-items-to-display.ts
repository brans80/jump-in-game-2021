import { UserAccountMapType } from "../user-types/user-map-items-to-dispay-type";


let userAccountItemsToDisplay: UserAccountMapType= new Map();

userAccountItemsToDisplay
.set(
  'displayName', {
    title: 'Display name',
    changeButton: true
  }
).set(
  'email', {
    title: 'Email',
    changeButton: false
  }
).set(
  'age', {
    title: 'Age',
    changeButton: true
  }
).set(
  'aboutUser', {
    title: 'About you',
    changeButton: true
  }
)

export default userAccountItemsToDisplay;