import { Button, createStyles, makeStyles, Theme } from '@material-ui/core';
import React from 'react';
import userAccountItemsToDisplay from './user-consts/user-map-items-to-display';
import { UserAccountItemsType } from './user-types/user-account-items-type';
import { UserAccountItemValToDisplayType } from './user-types/user-map-items-to-dispay-type';
import { userDetailsType } from './userType';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    userAccountDetails: {
      background: '#fff',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexWrap: 'wrap',
      boxSizing: 'border-box',
      padding: '0.4rem',
      marginBottom: '0',
      textAlign: 'center',
      margin: '0 auto',
    },

    userAccountDetailsItem: {

    }

  }),
);

type PropsType = {
  userDetails: userDetailsType,
}

export default function UserAccountDetails(props: PropsType) {
  const classes = useStyles();

  return (
    <div className='userAccountDetails'>
      {
        props.userDetails.admin && 
        <h3>Welcome admin!</h3>
      }
      {
        !!props.userDetails && 
        Array.from(userAccountItemsToDisplay).map((item: [UserAccountItemsType, UserAccountItemValToDisplayType]) => {
          let val = (props.userDetails as userDetailsType)[item[0]]
          let btnText = !!val? 'Change' : 'Add'
          return (
            <p key={item[0]}>
              <b>{item[1].title}</b> : <span>{val}</span>
              { 
                item[1].changeButton && <Button variant="outlined" color="secondary">{btnText}</Button>
              }
              
            </p>
          )
        })
      }
      
    </div>
  )
}
