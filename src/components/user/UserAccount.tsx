import { createStyles, makeStyles, Theme } from '@material-ui/core';
import React from 'react'
import { connect } from 'react-redux';
import { appStateType } from '../../store/store';
import { getUser, getUserDetails } from '../../store/user/user-selector/user-selector';
import userAccountItemsToDisplay from './user-consts/user-map-items-to-display';
import { UserAccountItemsType } from './user-types/user-account-items-type';
import { UserAccountItemValToDisplayType } from './user-types/user-map-items-to-dispay-type';
import UserAccountDetails from './UserAccountDetails';
import { userDetailsType } from './userType';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    userAccount: {
      background: '#fff',
      display: 'block',
      // alignItems: 'center',
      // justifyContent: 'center',
      // flexWrap: 'wrap',
      boxSizing: 'border-box',
      padding: '0.4rem',
      marginBottom: '0',
      textAlign: 'center',
      margin: '0 auto',
    },

  }),
);

type PropsType = {
  user: userDetailsType | null | undefined
}

export function UserAccount(props: PropsType) {
  const classes = useStyles();

  return (
    <div className={classes.userAccount}>
      {
        props.user &&
        <UserAccountDetails userDetails={props.user}/>
      }
    </div>
  )
}

const mapStateToProps = (state: appStateType) => ({
  user: getUserDetails(state)
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAccount)




