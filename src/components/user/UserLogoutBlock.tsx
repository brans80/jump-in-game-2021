import { Button, createStyles, makeStyles, Theme } from '@material-ui/core';
import React from 'react'
import { connect, useDispatch } from 'react-redux'
import { appStateType } from '../../store/store';
import { getUserDetails } from '../../store/user/user-selector/user-selector';
import { userLogoutThunk } from '../../store/user/user-thunk/user-logout-thunk';
import { userDetailsType } from './userType';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      background: '#fff',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexWrap: 'wrap',
      boxSizing: 'border-box',
      padding: '0.4rem',
      marginBottom: '0',
      textAlign: 'center',
      margin: '0 auto',
    },

    user: {
      display: 'block',
      marginRight: '0.5rem',
    }
  }),
);

type propsType = {
  userDetails?: userDetailsType | null,
}

export const UserLogoutBlock = (props: propsType) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  
  let logout = () => {
    userLogoutThunk(dispatch)
  }

  return (
    <div className={classes.root}>
      <span className={classes.user}>{props.userDetails?.displayName}</span>
      <Button  size="small" color="secondary" variant="contained" onClick={logout}>Log out</Button>
    </div>
  )
}

const mapStateToProps = (state: appStateType) => ({
  userDetails: getUserDetails(state)
})

export default connect(mapStateToProps, null)(UserLogoutBlock)
