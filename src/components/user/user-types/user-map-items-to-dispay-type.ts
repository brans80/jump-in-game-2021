import { UserAccountItemsType } from "./user-account-items-type";

export interface UserAccountItemValToDisplayType  {
  title: string,
  changeButton: boolean,
}

export type UserAccountMapType = Map<UserAccountItemsType, UserAccountItemValToDisplayType>