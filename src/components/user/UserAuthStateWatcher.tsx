import React from 'react';
import firebase from 'firebase';
import { db } from '../../firebase';
import { useDispatch } from 'react-redux';
import letChangeCurrentUser from '../../store/user/user-action/let-change-current-user/let-change-current-user';
import letChangePreloadingStatus from '../../store/game/game-action/let-change-preloading-status/let-change-preloading-status';
import { appDispatchType } from '../../store/store';
import letChangeUserDetails from '../../store/user/user-action/let-change-user-derails/let-change-user-details';
import { fetchToChangeUserDetails } from '../../store/user/user-thunk/fetch-to-channge-user-details-thunk';

let watchForUser = (dispatch: appDispatchType) => {
  db.collection("USERS");
  firebase.auth().onAuthStateChanged((user) => {
    // dispatch(letChangePreloadingStatus('userDetails', 'fetchUserDetails', false));
    if(!!user) {
      // dispatch(letChangeCurrentUser(user));

      fetchToChangeUserDetails(dispatch, user).then((res) => {
        // dispatch(letChangePreloadingStatus('userDetails', 'fetchUserDetails', false));
      })

      // var user = firebase.auth().currentUser;

    } else {
      // dispatch(letChangeCurrentUser(null));
      fetchToChangeUserDetails(dispatch, null).then((res) => {
        // dispatch(letChangePreloadingStatus('userDetails', 'fetchUserDetails', false));
      })

    }

    // dispatch(letChangePreloadingStatus(false));
  })
}

const UserAuthStateWatcher = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    watchForUser(dispatch);
    return () => {
      watchForUser(dispatch);
    }
  }, []);

  return (
    <></>
  )
}

export default UserAuthStateWatcher
