export type userDetailsType = {
  email: string | null,
  displayName?: string | null,
  photo?: string | null,
  age?: number,
  aboutUser?: string,
  admin?: boolean
}