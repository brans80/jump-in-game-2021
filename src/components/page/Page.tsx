import { Container, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { RouteParamsType } from '../../router/types/route-params-type';
import Header from '../header/Header';
import MainMenu from '../main-menu/MainMenu';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#8eba74',
    minHeight: '100vh',
    padding: '0 0 2rem',
  },

  title: {
    color: '#fff',
    fontWeight: 600,
  }
}));

type propsType = {
  title?: string,
  children?: JSX.Element
}

export default function Page(props: propsType) {
  const classes = useStyles();
  let params: RouteParamsType = useParams();

  return (
    <Container className={classes.root} maxWidth="md">
      <Header/>
      {
        props.title &&
        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>{props.title}</Typography>
      }
      {props.children && props.children}
    </Container>
  )
}
