import React from 'react'
import Page from './Page'

export default function CompetitorResultsPage() {
  return (
    <Page title="Competitor results"/>
  )
}
