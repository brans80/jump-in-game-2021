import React from 'react'
import withRedirectToHomeIfUserIsNull from '../../HOCs/withRedirectToHomeIfUserIsNull';
import Page from './Page'

export  function GamePage() {
  return (
    <Page title="Game">
      <>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga reiciendis impedit blanditiis tempore nulla, ipsam delectus maxime quos velit omnis.</p>
      </>
    </Page>
  )
}

export default withRedirectToHomeIfUserIsNull(GamePage);
