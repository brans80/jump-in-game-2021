import React from 'react'
import withRedirectToHomeIfUserIsNull from '../../HOCs/withRedirectToHomeIfUserIsNull';
import withRedirectToUrlPlusUser from '../../HOCs/withRedirectToUrlPlusUser';
import UserAccount from '../user/UserAccount';
import UserAccountDetails from '../user/UserAccountDetails';
import Page from './Page'

export  function AccountPage() {
  console.log('*****');
  return (
    <Page title="Your account">
      <>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga reiciendis impedit blanditiis tempore nulla, ipsam delectus maxime quos velit omnis.</p>
      <UserAccount/>
      </>
    </Page>
  )
}

export default withRedirectToHomeIfUserIsNull(withRedirectToUrlPlusUser(AccountPage));
