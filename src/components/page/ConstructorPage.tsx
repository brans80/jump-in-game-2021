import React from 'react'
import withRedirectToHomeIfUserIsNull from '../../HOCs/withRedirectToHomeIfUserIsNull';
import Constructor from '../constructor/Constructor';
import Page from './Page'

export function ConstructorPage() {
  return (
    <Page title="Constructor page">
      <Constructor/>
    </Page>
  )
}

export default withRedirectToHomeIfUserIsNull(ConstructorPage);
