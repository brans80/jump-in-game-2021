import React from 'react'
import withRedirectToHomeIfUserIsNull from '../../HOCs/withRedirectToHomeIfUserIsNull';
import GameSettings from '../game-settings/GameSettings';
import Page from './Page'

export function GameSettingsPage() {
  return (
    <Page title="Game settings">
      <GameSettings/>
    </Page>
  )
}

export default withRedirectToHomeIfUserIsNull(GameSettingsPage);