import React from 'react'
import withRedirectToHomeIfUserIsNull from '../../HOCs/withRedirectToHomeIfUserIsNull';
import AdminIntro from '../admin/AdminIntro';
import Page from './Page'

export function AdminPage() {
  return (
    <Page title="Admin page">
      <AdminIntro/>
    </Page>
  )
}

export default withRedirectToHomeIfUserIsNull(AdminPage);
