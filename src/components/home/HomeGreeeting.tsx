import { Button, Card, createStyles, makeStyles, Theme } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux';
import { dbUserType } from '../../firebase';
import { appStateType } from '../../store/store';
import { getUser, getUserDetails } from '../../store/user/user-selector/user-selector';
import CreateAccountForm from '../form/CreateAccountForm';
import LoginForm from '../form/LoginForm';
import ResetPasswordForm from '../form/ResetPasswordForm';
import { userDetailsType } from '../user/userType';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // background: '#fff',
      width: '24rem',
      minHeight: '12rem',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      flexWrap: 'wrap',
      boxSizing: 'border-box',
      padding: '1rem',
      marginBottom: '1.5rem',
      textAlign: 'center',
      margin: '0 auto',
    },
    margin: {
      margin: theme.spacing(1),
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }),
);

type formStateType = 0 | 1 | 2 | 3 | 4;

type propsType = {
  userDetails?: userDetailsType
}

export let HomeGreeting = (props: propsType) => {
  const [formState, setformState] = useState<formStateType>(0);
  const classes = useStyles();

  const isLogged = !!props.userDetails;

  useEffect(() => {

    if(isLogged) {
      changeformState(4)
    } 

  }, [props.userDetails])


  const showGreetingButtons = (formState === 0) ;
  const showLoginForm = (formState === 1) ;
  const showCreateForm = (formState === 2) ;
  const showResetForm = (formState === 3) ;
  const showGreetingText = (formState === 4);
  const showGoBackArrow = (formState === 1) || (formState === 2) || (formState === 3)
  

  let changeformState = (newFormState: formStateType) => {
    setformState((prevFormState: formStateType) => {
      return newFormState
    })
  }
  
  return (
    <Card className={classes.root}  variant="outlined">
      {
        showGreetingText && <h2>Welcome {props.userDetails?.displayName}</h2>
      }
      {
        showGoBackArrow &&
        <Button size="small" className={classes.margin} onClick={() => changeformState(0)}  startIcon={<ArrowBack />}>
          go back
        </Button>
      }

      {
        showLoginForm && <LoginForm />
      }

      {
        showCreateForm && <CreateAccountForm />
      }

      {
        showResetForm && <ResetPasswordForm />
      }

      {
        showGreetingButtons &&
        <>
          <div>
            <h5>I have an account:</h5>
            <Button size="small" className={classes.margin} onClick={() => changeformState(1)}>
              Log in
            </Button>
          </div>

          <div>
            <h5>I don't have an account:</h5>
            <Button size="small" className={classes.margin} onClick={() => changeformState(2)}>
              Create an account
            </Button>
          </div>
        </>
      }


      {
      (showLoginForm || showGreetingButtons) &&
        <div>
          <h5>I forgot the password:</h5>
          <Button size="small" className={classes.margin} onClick={() => changeformState(3)}>
            Reset password
          </Button>
        </div>
      }
    </Card>
  )
}

const mapStateToProps = (state: appStateType) => ({
  userDetails: getUserDetails(state)
})


const connector = connect(
  mapStateToProps,
  null
)

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(HomeGreeting)

// export default connect<{
//   userDetails: userDetailsType | null;
// }, null, {}, appStateType>(mapStateToProps, null)(HomeGreeting)
