import { Color } from "@material-ui/lab/Alert";


export type AlertObjectType = {
  title: string,
  text: string,
  severity: Color
}