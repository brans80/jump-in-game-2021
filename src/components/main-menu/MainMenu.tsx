import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import { routes } from '../../router/routes';
import { Button } from '@material-ui/core';
import { dbUserType } from '../../firebase';
import { userDetailsType } from '../user/userType';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '1rem 0',
      marginBottom: '1.5rem',

      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },

    button: {
      color: theme.palette.background.paper,
      borderColor: theme.palette.background.paper,
      '&.active': {
        color: 'yellow',
        borderColor: 'yellow',
        backgroundColor: 'green'
      },
    },
  }),
);

type propsType = {
  userDetails?: userDetailsType | null;
}

export default function MainMenu(props: propsType) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {
      routes.map((item) => {
        return (
          !item.offMenu &&
          <Button className={classes.button} activeClassName="active" component={ NavLink } to={item.url} variant="outlined" exact={item.exact} key={item.url} disabled={item.doDisabled && item.doDisabled(props.userDetails)}>
              {item.linkName}
          </Button>
        )
      }
     )
     }
    </div>
  )
}
