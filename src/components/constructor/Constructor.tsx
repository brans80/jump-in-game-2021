import { makeStyles } from '@material-ui/core';
import React from 'react'
import { useSelector } from 'react-redux';
import ConstructorSpecCellWrap from '../../spec-wraps/constructor/ConstructorSpecCellWrap';
import { appStateType } from '../../store/store';
import CellGrid from '../cells/CellGrid';
import { CellMatrixType } from '../cells/types/cell-type';
import ConstructorSelectionBar from './ConstructorSelectionBar';

const useStyles = makeStyles((theme) => ({
  constructorRoot: {
    backgroundColor: '#fff',
    display: 'flex',
    padding: '1rem',
    justifyContent: 'center',
  },

  constructorGrid: {
    display: 'block',
    marginRight: '1.5rem',
  },

  constructorBar: {
    display: 'block',
    width: '10rem',
  }
}));

export default function Constructor() {
  const classes = useStyles();

  const matrix: CellMatrixType = useSelector((state: appStateType) => {return state.constructorReducer.matrix});

  React.useEffect(() => {
   console.log('MATRIX WAS CHANGED');
  }, [matrix])

  return (
    <div className={classes.constructorRoot}>
      <div className={classes.constructorGrid}>
        <CellGrid matrix = {matrix} cellComponent = {ConstructorSpecCellWrap}/>
      </div>
      <div className={classes.constructorBar}>
        <ConstructorSelectionBar />
      </div>
      
    </div>
  )
}
