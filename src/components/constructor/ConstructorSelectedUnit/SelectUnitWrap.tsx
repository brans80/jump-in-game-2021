import { makeStyles } from '@material-ui/core';
import React, { FC } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import letConstructorChangeSelectedUnit from '../../../store/constructor/action/let-constructor-change-selected-unit/let-constructor-change-selected-unit';
import { appStateType } from '../../../store/store';
import Barrier from '../../cells/Barrier';
import { foxCollection, mushroomsCollection } from '../../cells/constants/barriers';
import { rabbitsCollection } from '../../cells/constants/rabbits';
import Rabbit from '../../cells/Rabbit';
import { CellContentType } from '../../cells/types/cell-type'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    position: 'relative',
  },

  unitStack: {
    position: 'relative',
    backgroundColor: 'transparent',
    display: 'block',
    width: '5.2rem',
    height: '5.2rem',
    marginBottom: '1rem',
    cursor: 'pointer'
  },

  unitRestNumber: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    border: '1px solid #444',
    backgroundColor: '#fff',
    position: 'absolute',
    width: '1.6rem',
    height: '1.6rem',
    top: '-0.2rem',
    right: '-0.2rem',
    fontSize: '1rem',
    lineHeight: '0',
    color: 'red',
    fontWeight: 600,
  },

  selectedUnit: {
    position: 'relative',
    backgroundColor: 'transparent',
    display: 'block',
    width: '4rem',
    height: '4rem',
    marginLeft: '1rem',
    marginTop: '0.8rem',
    cursor: 'default',
  },

  closeBtn: {
    position: 'absolute',
    backgroundColor: '#fff',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2.2rem',
    height: '2.2rem',
    right: '-1rem',
    top: '-1rem',
    color: 'red',
    border: 'none',
    lineHeight: 0,
    fontSize: '1.6rem',
    cursor: 'pointer',
    borderRadius: '50%',

    '&:hover' : {
      color: '#999'
    }
  }
}));

type PropsType = {
  unitsArray: CellContentType[];
  UnitComponent: FC<any>
}

export default function SelectUnitWrap(props: PropsType) {
  const classes = useStyles();
  const [unitsArray, chageUnitsArrayState] = React.useState<CellContentType[]>(props.unitsArray);
  const selectedUnit  = useSelector((state: appStateType) => state.constructorReducer.selectedUnit);
  const dispatch = useDispatch()
  let firstUnitInStack = unitsArray[0];
  const refCapSelectedUnit = React.useRef(unitsArray[0])

  let unitStackClickHandler = (e: React.MouseEvent<HTMLElement>): void => {
    e.stopPropagation();
    e.preventDefault();

    if(firstUnitInStack && !selectedUnit) {
      chageUnitsArrayState((prevUnitsArr) => {
        return [...prevUnitsArr.slice(1)]
      })

      dispatch(letConstructorChangeSelectedUnit(firstUnitInStack));
    }
  }

  let unitRemoveClickHandler = (e: React.MouseEvent<HTMLElement>): void => {
    e.stopPropagation();
    e.preventDefault();

    if(selectedUnit) {
      chageUnitsArrayState((prevUnitsArr) => {
        return [...prevUnitsArr, selectedUnit ]
      });
    }

    dispatch(letConstructorChangeSelectedUnit(null))
  }

  return (
    <div className = {classes.root}>
      <div 
        className = {classes.unitStack} 
        onClick = {unitStackClickHandler}
      >
        <span className = {classes.unitRestNumber}>{unitsArray.length}</span>
        {unitsArray.length > 0 ? <props.UnitComponent {...firstUnitInStack}/> : <props.UnitComponent {...refCapSelectedUnit.current}/>} 
        
      </div>
      {!!selectedUnit  && (selectedUnit?.name === refCapSelectedUnit.current.name) &&
        <div className = {classes.selectedUnit}>
          <button className = {classes.closeBtn} type="button" onClick={unitRemoveClickHandler}>&#10005;</button>
          <props.UnitComponent {...selectedUnit}/>
        </div>
      }
    </div>
  )
}

export function SelectUnitWrapRabbit () {
  return (
    <SelectUnitWrap unitsArray = {rabbitsCollection} UnitComponent = {Rabbit} />
  )
}

export function SelectUnitWrapMushroom () {
  return (
    <SelectUnitWrap unitsArray = {mushroomsCollection} UnitComponent = {Barrier} />
  )
}

export function SelectUnitWrapFox () {
  return (
    <SelectUnitWrap unitsArray = {foxCollection} UnitComponent = {Barrier} />
  )
}
