import { makeStyles } from '@material-ui/core';
import React from 'react'
import {  SelectUnitWrapFox, SelectUnitWrapMushroom, SelectUnitWrapRabbit } from './ConstructorSelectedUnit/SelectUnitWrap';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: 'transparent',
    display: 'block'
  },
}));

export default function ConstructorSelectionBar() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h2>Selection BAr</h2>
      <hr/>
      {/* <ConstructorSelectItemRabbitWrap/> */}
      <SelectUnitWrapRabbit/>
      <SelectUnitWrapMushroom/>
      <SelectUnitWrapFox/>
      {/* <ConstructorSelectRabbitWrap />
      <ConstructorSelectBarrierWrap /> */}
    </div>
  )
}
