import React from 'react'
import { connect } from 'react-redux'
import { getPreloadingStatus } from '../../store/game/game-selector/game-selector'
import { appStateType } from '../../store/store'
import Preloader from './Preloader'

export const PreloaderWrap = (props: {preloadingStatus: boolean}) => {

  React.useEffect(() => {
    
    console.log(props.preloadingStatus);
    
  }, [props.preloadingStatus])
  return (
    <>
      {
        props.preloadingStatus && <Preloader diameter={200}/>
      }
    </>
  )
}

PreloaderWrap.defaultProps = {
  preloadingStatus: false
}

const mapStateToProps = (state: appStateType) => ({
  preloadingStatus: getPreloadingStatus(state)
})


export default connect(mapStateToProps, null)(PreloaderWrap)
