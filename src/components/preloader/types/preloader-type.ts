export type PreloaderItemType = 'user' | 'userDetails' | 'levels';

export type PreloaderType = Map<PreloaderItemType, string[]>




