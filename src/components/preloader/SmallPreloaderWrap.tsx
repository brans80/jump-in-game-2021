import React from 'react'
import { connect } from 'react-redux'
import { getPreloadingStatus } from '../../store/game/game-selector/game-selector'
import { appStateType } from '../../store/store'
import Preloader from './Preloader'

export const SmallPreloaderWrap = () => {


  return (
      <Preloader diameter={80}/>
  )
}


