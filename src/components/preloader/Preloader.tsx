import React from 'react';
import { makeStyles, createStyles, withStyles, Theme } from '@material-ui/core/styles';
import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress';

const useStylesFacebook = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'absolute',
      zIndex: 10,
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    circlesWrap: {
      position: 'relative',
    },
    bottom: {
      color: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    top: {
      color: '#1a90ff',
      animationDuration: '1200ms',
      position: 'absolute',
      left: 0,
    },
    circle: {
      strokeLinecap: 'round',
    },
  }),
);

type PropsI = {
  diameter: number
}


 function Preloader(props: PropsI) {
  const classes = useStylesFacebook();


  return (
    <div className={classes.root}>
      <div className={classes.circlesWrap}>
        <CircularProgress disableShrink size={props.diameter}/>
      </div>
    </div> 
  );
}

// Preloader.defaultProps = {
//   diameter: 200
// }

export default Preloader


